class Tab {
  constructor(tabDataArr) {
    this.tabDataArr = tabDataArr;
    this.root = document.createElement('div');
    this.root.classList.add('tab__root');
    this.root.tabIndex = 0;
    this.active = 0;
    this.focusParams = { focus: false, itemId: 0 };
  }
  setActiveTab(index) {
    const { tabDataArr, root } = { ...this };
    const curActiveTab = root.getElementsByClassName('_current')[0];
    const tabDataElem = root.getElementsByClassName('tab__data')[0];
    if (curActiveTab)
      curActiveTab.classList.remove('_current');
    tabDataArr[index].item.classList.add('_current');
    tabDataElem.innerText = tabDataArr[index].data;
  }
  setFocus() {
    const { tabDataArr } = { ...this };
    const curFocus = this.root.getElementsByClassName('_focus')[0];
    if (this.focusParams.itemId >= 0) {
      const item = tabDataArr[this.focusParams.itemId].item;
      const parentBRC = item.parentElement.getBoundingClientRect();
      const { width, height, top, left } = item.getBoundingClientRect();
      console.log(item.getBoundingClientRect());
      curFocus.textContent = item.textContent;
      curFocus.style.left = (left - parentBRC.left-4) + 'px';
      curFocus.style.width = (width+3) + 'px';
      curFocus.style.height = (height+3) + 'px';
    }
  }
  render() {
    const { tabDataArr, root } = { ...this };
    const items = document.createElement('div');
    items.classList.add('tab__items');
    root.appendChild(items);
    const tabDataElem = document.createElement('div');
    tabDataElem.classList.add('tab__data');
    const focusElem = document.createElement('div');
    focusElem.classList.add('_focus');
    root.appendChild(tabDataElem);
    root.addEventListener('mousedown', (event) => {
      event.preventDefault();
      return false;
    });
    root.addEventListener('focus', (event) => {
      this.focusParams.focus = true;
      this.focusParams.itemId = 0;
      focusElem.style.display = 'inline-block';
      this.setFocus();
    });
    root.addEventListener('keydown', (event) => {
      const itemId = this.focusParams.itemId;
      if (this.focusParams.focus) {
        switch (event.code) {
          case 'Enter':
            this.setActiveTab(itemId);
            break;
          case 'ArrowRight':
            this.focusParams.itemId = itemId + 1 < tabDataArr.length ? itemId + 1 : 0;
            this.setFocus();
            break;
          case 'ArrowLeft':
            this.focusParams.itemId = itemId - 1 >= 0 ? itemId - 1 : tabDataArr.length - 1
            this.setFocus();
            break;
          default:
            break;
        }
      }
    });
    root.addEventListener('blur', (event) => {
      focusElem.style.display = 'none';
      this.focusParams.focus = false;
      this.focusParams.itemId = -1;
      this.setFocus();
    });
    items.appendChild(focusElem);
    for (let i = 0; i < tabDataArr.length; i++) {
      const tabData = tabDataArr[i];
      tabData.id = i;
      tabData.item = document.createElement('div');
      tabData.item.classList.add('tab__item');
      tabData.item.innerText = tabData.title;
      tabData.item.addEventListener('click', () => {
        this.setActiveTab(tabData.id);
      })
      items.appendChild(tabData.item);
    }
    this.setActiveTab(0);
    return this.root;
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render())
